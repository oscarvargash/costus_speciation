# Patterns of speciation are similar across mountainous and lowland regions for a Neotropical plant radiation (Costaceae: Costus)  #

This is the repository for the paper "Patterns of speciation are similar across mountainous and lowland regions for a Neotropical plant radiation (Costaceae: Costus)" published in the journal Evolution.

The repository is organized in three main folders comprising the three main set of analyses presented in the paper.

Folders are organized sequentially according to the order they appear in the manuscript, with titles indicating the specific analysis associated with the file contents.

Inside each one of the folders you will find the code used for every component of the pipeline and the main inputs and outputs

Code in the repository have the following suffixes:
.sh 	= to be executed in bash

.r .R	= to be executed in R

.py		= to be executed in python 2


Fasta and tree files have additive inserts in the name to indicate their processing:

.mafft 	= aligned with MAFFT

.norr	= outgroups removed and tree is rooted in ingroup

.oc		= columns with less than 95% occupancy were removed from the alignment

.pr		= tree pruned to match climatic data occupancy

.rd		= alignment/tree reduced to monophyletic taxa

.rr		= tree was re-rooted

.spur 	= outliers individual sequences were removed with TrimAl

.trim 	= sections for the alignment were trimmed with TrimAl

s.ts 	= extreme long branches and/or taxa with long branches flagged by TreeShrink excluded






