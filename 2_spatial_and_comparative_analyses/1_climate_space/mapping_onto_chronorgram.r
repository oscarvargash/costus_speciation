

setwd("/Users/oscar/Dropbox/Costus_local/phylogeny_paper/climate_space")
dir()

library(phytools)
# read tree and plot

tree <- read.tree("chronogram_v8_2.no.rs.rd.tre")
plot(tree,cex=0.4, show.node.label=T)
axisPhylo()

# read a matrix with climatic characters

data <-  read.csv("species.clim.means_v5.csv",row.names=1)
cat <- as.matrix(read.csv("species.cat.csv", row.names = 1, skip=1, header=F))

tip.col <- cat[,1]
tip.col[tip.col=="mountain"] <- "#FF7E76"
tip.col[tip.col=="amazonian"] <- "#6676FF"
cols<-c(tip.col[tree$tip.label],rep("black",tree$Nnode))
names(cols)<-1:(length(tree$tip)+tree$Nnode)

pdf(file="costus_pc1_pc2.pdf",width=6,height=6,paper='special') 
phylomorphospace(tree,label="off", data[,c(7,8)], xlab="PC1", ylab="PC2", control=list(col.node=cols))
dev.off()

pdf(file="costus_pc1_pc2_labels.pdf",width=6,height=6,paper='special') 
phylomorphospace(tree,label="radial", data[,c(7,8)], xlab="PC1", ylab="PC2", control=list(col.node=cols))
dev.off()

# trying to find 95% envelopes
hist(data$PC2)
hist(data$PC1)
