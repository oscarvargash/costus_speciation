
library(ggplot2)
library(phytools)

setwd("/Users/oscar/Dropbox/Costus_local/phylogeny_paper/pollination")
dir()

# perform chi-square test to compare catergories and pollination
# syndromes in extant species

poll <- read.csv("costus_pollination_v2.csv", row.names = 1)
t <- table(poll$category, poll$syndrome)
test <- chisq.test(t)
capture.output(test, file="extant_species_test.txt") 

# perform chi-square test to number of transitions in catergories
# on all edges of the tree

# draw tree with node number for referece
tree <- read.tree("chronogram_v8_2.no.rs.ld.tre")
plot(tree, cex=0.5)
nodelabels(cex=0.5)

# import table

trans <- read.csv("all_edges_pollination_v2.csv", row.names = 1)
t2 <- table(trans$category, trans$transition)
test2 <- chisq.test(t2, simulate.p.value = TRUE)
capture.output(test2, file="transition_test.txt") 

# make mosaic plot for each group
pdf(file="SupFigure6.pdf", width=9, height=4.5)
par( mfrow=c(1,2),mar=c(4,2,4,2))
colnames(t) = c("bee","hummingbird")
mosaicplot(t,col=c("white","lightgrey"),main="", sub="", cex.axis=1.2,
           border =NULL)
text( 0.17,0.65, t[1,1], cex = 1.1)
text( 0.17,0.14, t[1,2],cex = 1.1)
text(0.64,0.75, t[2,1],cex = 1.1)
text( 0.64,0.25, t[2,2], cex = 1.1)
mtext("A. Extant species comparisons",side=3, line=1,adj=0, cex=1.3)

colnames(t2) = c("no transition","to hummingbird")
mosaicplot(t2,col=c("white","lightgrey"),main="", sub="", cex.axis=1.2,
           border =NULL)
text( 0.168,0.53, t2[1,1], cex = 1.1)
text( 0.168,0.002, t2[1,2],cex = 1.1)
text(0.64,0.55, t2[2,1],cex = 1.1)
text( 0.64,0.015, t2[2,2], cex = 1.1)
mtext("B. Transitions on phylogeny",side=3, line=1,adj=0, cex=1.3)
dev.off()




