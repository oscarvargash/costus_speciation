#install.packages("phytools")
#install.packages("maps")
#install.packages("ape")
#install.packages()

setwd("/Users/oscar/Dropbox/Costus_local/phylogeny_paper/pollination_mapping")
dir()

library(phytools)
library(ggplot2)

# read tree and plot

tree <- read.nexus("test.tre")
tree <- ladderize(tree, right = T)
plot(tree,cex=0.7, show.node.label=T)
axisPhylo()

#Make list of taxa and drop outgroups
sort(tree$tip.label)
tree <- drop.tip(tree, c("Canna_indica","Canna_sp","Costus_dubius_98074","Costus_lucanusianus_99103","Costus_afer_98068","Curcuma_longa","Dichorisandra_thyrsiflora","Heliconia_sp","Maranta_leuconeura","Musa_basjoo","Orchidantha_fimbriata","Strelitzia_reginae","Typha_latifolia","Zingiber_officinale"))

write.tree(tree, file = "ingroup.tre")

plot(tree,cex=0.7, show.node.label=T)
axisPhylo()


# read charater states
poll <- read.csv("costus_pollination.csv", row.names = 1)
poll <- as.matrix(poll)[,1]

# perform simulations for ancestral reconstruction

costus_trees <- make.simmap(tree,poll,model="ARD",nsim=1000)
transitions <- as.data.frame(countSimmap(costus_trees)) 
obj <- summary(costus_trees)

hist(transitions$Tr.B.H)

bee_2_humm <- as.data.frame(transitions$Tr.B.H)
names(bee_2_humm) = "n"
humm_2_bee <- as.data.frame(transitions$Tr.H.B)
names(humm_2_bee) = "n"

bee_2_humm$type <- 'bee_to_hummingbird'
humm_2_bee$type <- 'hummingbird_to_bee'

transitions2 <- rbind(bee_2_humm, humm_2_bee)

ggplot(transitions2, aes(n, fill=type)) + geom_histogram(binwidth=1)
ggsave("transitions.pdf", plot = last_plot())

# define colors

colors<-setNames(c("yellow","red"),colnames(obj$ace))

# plot and save in pdf

pdf(file="costus_pollination2.pdf",width=6,height=6,paper='special') 

plot(obj,ftype="i",lwd=1,fsize=0.6,ylim=c(-4,Ntip(tree)),
     cex=c(0.6,0.3),colors=colors)
add.simmap.legend(x=0,y=8,colors=colors,prompt=FALSE,
                  horizontal=FALSE,shape="circle",fsize=0.9)
axisPhylo()

dev.off()

#  plot and save in EPS

setEPS()
postscript("costus_pollination.eps", horizontal = FALSE, onefile = FALSE, paper = "special",width=5,height=5 )

plot(obj,ftype="i",lwd=1,fsize=0.6,ylim=c(-4,Ntip(tree)),
     cex=c(0.6,0.3),colors=colors)
add.simmap.legend(x=0,y=8,colors=colors,prompt=FALSE,
                  horizontal=FALSE,shape="circle",fsize=0.9)
axisPhylo()
dev.off()
