alt	null	LnLalt	LnLnull	DFalt	DFnull	DF	Dstatistic	pval	test	tail	AIC1	AIC2	AICwt1	AICwt2	AICweight_ratio_model1	AICweight_ratio_model2
1	DEC+J	DEC	-88	-92.01	3	2	1	8.01	0.0046	chi-squared	one-tailed	182	188	0.95	0.047	20.22	0.049
2	DIVALIKE+J	DIVALIKE	-88.68	-90.65	3	2	1	3.94	0.047	chi-squared	one-tailed	183.4	185.3	0.73	0.27	2.64	0.38
3	BAYAREALIKE+J	BAYAREALIKE	-93.83	-113.2	3	2	1	38.65	5.1e-10	chi-squared	one-tailed	193.7	230.3	1.00	1.1e-08	9.10e+07	1.1e-08
