# Climate PCA and Sister Pair Niche Equivalencies
# last modified May 23, 2020 Dena Grossenbacher
# Vargas et al. running title: Ecogeographic speciation in Costus

library(dismo) #raster manipulation
library(ecospat) # climate pca and niche equivalency
library(factoextra) #biplot
library(distances) # euclidean distances
library(ContourFunctions) #make title with multiple colors
library(weights) #weighted chi squared

#import climate raster stack (created using getclimateraster.R; stacked in this order: bio1,bio4,bio12,bio15)
envs = stack("output/envs.tif") 

###########################################
# Projecting occurrence data
###########################################

#import occurrence data
occurrence_data <- read.csv("cleaned.occurrences.csv")

# Project to Coordinate system
occurrence_data.spdf <- SpatialPointsDataFrame(data.frame(occurrence_data$decimalLongitude, 
                                                          occurrence_data$decimalLatitude), 
                                               data=occurrence_data,
                                               proj4string=CRS("+proj=aea +lat_1=-5 +lat_2=-42 +lat_0=-32 +lon_0=-60 +x_0=0 +y_0=0 +ellps=aust_SA +units=m +no_defs"))

# Create new data frame with your new coordinates 
new_coordinates <- data.frame(species = occurrence_data[,c("acceptedScientificName")], #May need to change column number
                              coordinates(occurrence_data.spdf))

# Make a SpatialPointDataFrame for all occurrences
all.spdf <- SpatialPointsDataFrame(data.frame(new_coordinates$occurrence_data.decimalLongitude, 
                                              new_coordinates$occurrence_data.decimalLatitude), 
                                   data=new_coordinates,
                                   proj4string=CRS("+proj=aea +lat_1=-5 +lat_2=-42 +lat_0=-32 +lon_0=-60 +x_0=0 +y_0=0 +ellps=aust_SA +units=m +no_defs"))


# plot to make sure everything is working
plot(envs[[1]])
points(all.spdf)

###########################################
# Extract climate data for background occurrences
###########################################

# Create a RasterLayer with the extent of occurrences
r <- raster(envs)

# Set the resolution of the cells to 0.01 latitudinal degree (ca. 1x1km)
res(r) <- 0.01

# Expand the extent of the RasterLayer a little
r <- extend(r, extent(r)+1)

# Sample all species occurrences to get background grid cells
# Sample each grid cell only once
background.occ <-gridSample(all.spdf, r, n=1) 
df.background.occ <- data.frame(background.occ)

# Extract bioclim variables for background grid cells
# Note that this extraction yields ~2% NAs that vary slightly each time this is run
my.background.values <- data.frame(extract(envs,background.occ))
my.background.values$acceptedScientificName <- "background"
nrow(my.background.values)
my.background.values.noNA <- na.omit(my.background.values) #drop points with NA
nrow(my.background.values.noNA)
df.downsampled <- my.background.values.noNA

###########################################
# Extract climate data for each species' occurrences
###########################################

# for each species, sample each occupied grid cell only once
sp <- sort(unique(as.character(occurrence_data$acceptedScientificName))) 
for (i in 1:length(sp)) {
  mysp = sp[i]
  my.sp.occ <- gridSample(subset(all.spdf, species==mysp), r, n=1)
  my.sp.occ <- data.frame(my.sp.occ)
  
  # Extract bioclim variables for species grid cells
  # Note that this extraction yields ~2% NAs that vary slightly each time this is run
  my.values <- extract(envs,my.sp.occ)
  df.my.values = data.frame(my.values)
  df.my.values$acceptedScientificName <- mysp
  head(df.my.values)
  df.my.values <- na.omit(df.my.values) #drop points with NA
  df.downsampled <- rbind(df.my.values,df.downsampled)
}

colnames(df.downsampled) <- c("bio1","bio4","bio12","bio15", "acceptedScientificName")

#######################################################
###### Principal components analysis
####################################################### 
# note that dudi.pca produces slightly different absolute scores each time it is executed

df.background = subset(df.downsampled, acceptedScientificName=="background")
nrow(df.background) # number of background points used to construct the PCA

genus.df = subset(df.downsampled, !acceptedScientificName=="background")

#use background points to make PCA environment
w<-c(rep(0,nrow(genus.df)),rep(1,nrow(df.background))) #vector of weight, 0 for the occurences, 1 for the background points
pca.cal <- dudi.pca(df.downsampled[,c("bio1","bio12","bio4","bio15")], row.w = w, center = TRUE, scale = TRUE, scannf = FALSE, nf = 2)
#save(output/pca.cal, file="pca.cal")

#pdf("output/Figure_PCAbiplot.pdf", width=4, height=4)
plot(pca.cal$c1$CS1, pca.cal$c1$CS2, 
       xlab="Climate PC1 ", ylab="Climate PC2 ", ylim=c(-0.1,1),
       pch=20, bty="n", axes=TRUE, cex=1,cex.lab=1)
abline(h=0, col="grey"); abline(v=0, col="grey")
text(pca.cal$c1$CS1 , pca.cal$c1$CS2,, labels=rownames(pca.cal$c1),cex=0.8, pos=c(4,4,2,2))
#dev.off()

#bind PC scores to df.downsampled
df.downsampled=cbind(df.downsampled,pca.cal$li)

#plot background points
df.background = subset(df.downsampled, acceptedScientificName=="background")
plot(df.background$Axis1,df.background$Axis2, xlab="climate axis 1", ylab="climate axis 2", pch=20, col="darkgray")
abline(h=0, col="darkgray"); abline(v=0, col="darkgray")

##########################################
# Get species mean climate values and number of grid cells occupied
##########################################

#get species mean values and save
spsum <- aggregate(list(df.downsampled[,c("bio1","bio4","bio12","bio15","Axis1","Axis2")]), by = list(df.downsampled$acceptedScientificName), mean)
names(spsum)[names(spsum) == 'Group.1'] <- 'acceptedScientificName'

#get number of grid cells occupied for each species
df.ngrids <- as.data.frame(table(df.downsampled$acceptedScientificName))
colnames(df.ngrids) <- c("acceptedScientificName", "n")
spsum <- merge(spsum,df.ngrids, by="acceptedScientificName")

#write.csv(spsum, "output/species.clim.means.csv",row.names=FALSE)
#then modified by hand to add species names as they appear in the phylogeny and region (mountain influenced vs amazonian)

##########################################
# Make climate PC1xPC2 graphs for all species pairs
##########################################

# import sister pairs
sis.sum <- read.csv("sister.pairs.csv")
sis.sum <- subset(sis.sum, !b.geo=="") #keep only pairs with geographic data

####  make plots for all sisters with lines drawn indicating euclidean distance ###
#pdf("Figure_SisterNiches.pdf",  width=9,height=12)  
par(mar=c(5, 4, 1.4, 0.2), mfrow=c(3,2),cex.lab=1) 
for (i in 1:nrow(sis.sum)) {
  plot(df.downsampled$Axis1,df.downsampled$Axis2, xlab="climate axis 1", ylab="climate axis 2", pch=20, col="darkgray")
  abline(h=0, col="darkgray"); abline(v=0, col="darkgray")
  sis.b = subset(df.downsampled, acceptedScientificName %in% sis.sum$b.geo[i])
  sis.a = subset(df.downsampled, acceptedScientificName %in% sis.sum$a.geo[i])
  points(sis.a$Axis1,sis.a$Axis2, col="black", pch=1)
  points(sis.b$Axis1,sis.b$Axis2, col="red", pch=1)
  #draw connector line for euclidean distance
  segments(mean(sis.a$Axis1),mean(sis.a$Axis2), mean(sis.b$Axis1), mean(sis.b$Axis2), lwd=2, col="green")
  multicolor.title(c(as.character(sis.sum$a.label[i])," - ",as.character(sis.sum$b.label[i])),c("black","darkgray", "red"))
}
#dev.off()


####################################################################
###### Get sister pair mean climate PC1, PC2 and euclidean distances
####################################################################
for (i in 1:nrow(sis.sum)) {
  my.a <- subset(spsum, acceptedScientificName==as.character(sis.sum$a.geo[i]))
  my.b <- subset(spsum, acceptedScientificName==as.character(sis.sum$b.geo[i]))
  sis.sum$a.pc1[i] <- my.a$Axis1
  sis.sum$a.pc2[i] <- my.a$Axis2
  sis.sum$b.pc1[i] <- my.b$Axis1
  sis.sum$b.pc2[i] <- my.b$Axis2
}

# get euclidean distance in climate PC1 and PC2 space
euc<- c()
for(i in 1:nrow(sis.sum)) {
  my.matrix <- matrix(c(sis.sum$a.pc1[i],sis.sum$b.pc1[i],sis.sum$a.pc2[i],sis.sum$b.pc2[i]), nrow=2)
  my.euc <- distances(my.matrix)[1,2]
  euc <- c(euc, my.euc)
}
sis.sum$euc.dist <- euc

##############################################
###### Run sister pair Niche Equivalency Tests
##############################################
# Niche equivalency: tests the hypothesis that the niche of two species are 
# equivalent (i.e. the observed overlap is similar to simulated overlap when randomly 
# reallocating the occurrences of both species among the joint distribution of occurrences)

#get number of grid cells occupied for sister pairs
n.sis.a <- c()
n.sis.b <- c()
for (i in 1:nrow(sis.sum)) {
  sis.a = subset(df.downsampled, acceptedScientificName==as.character(sis.sum$a.geo[i]))
  sis.b = subset(df.downsampled, acceptedScientificName==as.character(sis.sum$b.geo[i]))
  my.n.sisA <- nrow(sis.a)
  my.n.sisB <- nrow(sis.b)
  n.sis.a <- c(n.sis.a, my.n.sisA)
  n.sis.b <- c(n.sis.b, my.n.sisB)
}
sis.sum$n.sis.a <- n.sis.a
sis.sum$n.sis.b <- n.sis.b

#drop pairs with fewer than 5 grid cells occupied for assesing niche equivalency
sis.sum.reduced <- subset(sis.sum, n.sis.a > 4 & n.sis.b > 4)

#make columns for niche equivalency stats
sis.sum.reduced$D <- ""
sis.sum.reduced$pvalue.D <- ""
sis.sum.reduced$I <- ""
sis.sum.reduced$pvalue.I <- ""

#get niche equivalency stats
for (i in 1:nrow(sis.sum.reduced)) {
  mysis = as.character(sis.sum.reduced$sis[i])
  sis.a = subset(df.downsampled, acceptedScientificName==as.character(sis.sum.reduced$a.geo[i]))
  sis.b = subset(df.downsampled, acceptedScientificName==as.character(sis.sum.reduced$b.geo[i]))
  scores.sp1 <- data.frame(sis.a$Axis1,sis.a$Axis2) #scores for sp1
  scores.sp2 <- data.frame(sis.b$Axis1,sis.b$Axis2) #scores for sp2
  
  # calculation of occurence density and test of niche equivalency and similarity
  z1 <- ecospat.grid.clim.dyn (pca.cal$li, pca.cal$li, scores.sp1,R=100)
  z2 <- ecospat.grid.clim.dyn(pca.cal$li, pca.cal$li, scores.sp2,R=100)
  my.test = ecospat.niche.equivalency.test (z1, z2, rep=100, alternative="lower", ncores = 1)
  
  sis.sum.reduced$D[i] <- round(my.test$obs$D,digits=4)
  sis.sum.reduced$pvalue.D[i] <- round(my.test$p.D,digits=4)
  sis.sum.reduced$I[i] <- round(my.test$obs$I,digits=4)
  sis.sum.reduced$pvalue.I[i] <- round(my.test$p.I,digits=4)
}

##############################################
###### Merge sister pair results: PC and Niche Equivalency Tests
##############################################
sis.sum.reduced$names <- paste(sis.sum.reduced$a, sis.sum.reduced$b, sep=" ")
sis.sum$names <- paste(sis.sum$a, sis.sum$b, sep=" ")
sis.sum.reduced <- sis.sum.reduced[,c("names","D","pvalue.D","I","pvalue.I")]
sis <- merge(sis.sum, sis.sum.reduced, by="names", all=T)
#write.csv(sis, "output/sister.pairs.climate.csv",row.names=FALSE)

##############################################
###### Does the proportion of non-equivalent sisters vary by region?
##############################################
sis$non.equiv <- sis$pvalue.D < 0.05
wtd.chi.sq(sis$pvalue.D, sis$region, weight=sis$n, na.rm=T)
