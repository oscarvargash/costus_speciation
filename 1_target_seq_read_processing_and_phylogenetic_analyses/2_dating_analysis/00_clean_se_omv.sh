echo "Usage:"
echo ""
echo "bash clean_pe.sh raw_data_foldername R1_ending"
echo ""
echo "* raw paired end data has no uniform ending for filenames R1_ending may be _R1.fq.gz, _R1_.fastq.gz, _1.fastq.gz, etc"
echo ""
echo "First, adapters are removed with fastp"
echo "Second, PhiX and sequencing artifacts are filtered out, then quality is trimmed using bbduk.sh"
echo "bbduk.sh uses PHRED algorithm, filtering based on error probability instead of raw quality score"
echo ""
echo ""
echo "#########################################"
echo "# STEP 1: Remove adapters with bbduk.sh #"
echo "#########################################"
echo ""

## Create folder for decontaminated reads
mkdir 01_adapters_removed

## Switch to input folder given by first argument $1
cd $1

## R1 ending pattern is given in $2
patternI1=$2
read1="_R1.fastq.gz"
readH="_R#.fastq.gz"

## RAM for bbduk.sh
RAM="-Xmx8g"

## Remove adapters with bbduk
for file in *$patternI1; do
	echo "Removing adapters: "${file//$patternI1/}
	time bbduk.sh $RAM in=${file//$patternI1/${patternI1//_R1/_R#}} out=../01_adapters_removed/${file//$patternI1/$readH} \
	ref=$HOME/omv_software/bbmap/resources/adapters.fa \
	ktrim=r k=21 mink=11 hdist=2 tpe tbo ziplevel=1 minlength=21 \
	stats=../01_adapters_removed/${file//$patternI1/.bbduk.adapters.stats.txt} \
	&>../01_adapters_removed/${file//$patternI1/.bbduk.adapters.log.txt}
	echo ""
	echo ""
done

echo "#####################################################################"
echo "# STEP 2: Remove contaminants and quality filter/trim with bbduk.sh #"
echo "#####################################################################"

## Create folder for clean reads
cd ..
mkdir 02_clean
cd 01_adapters_removed

## Remove contaminants with bbduk.sh 
for file in *$read1; do
	echo "Cleaning: "${file//$read1/}
	time bbduk.sh $RAM in1=${file//$read1/$readH} out=../02_clean/${file//$read1/$readH} \
	ref=$HOME/omv_software/bbmap/resources/phix174_ill.ref.fa,$HOME/omv_software/bbmap/resources/sequencing_artifacts.fa,$HOME/omv_software/bbmap/resources/UniVec.fa \
	k=31 hdist=1 qtrim=lr trimq=8 maq=12 minlength=21 maxns=5 ziplevel=5 \
	stats=../02_clean/${file//$read1/.contaminants.stats.txt} \
	&>../02_clean/${file//$read1/.cleaning.log.txt}
	echo ""
	echo ""
done

echo "######################################"
echo "# STEP 3: Quality checks with fastqc #"
echo "######################################"

cd ../02_clean

# Run fastqc
mkdir 00_fastqc_before 01_fastqc_after
cd ../$1
fastqc -o ../02_clean/00_fastqc_before --nogroup -t 12 *$patternI1 *$patternI2
rm ../02_clean/00_fastqc_before/*.zip

cd ../02_clean
fastqc -o 01_fastqc_after --nogroup -t 12 *.fastq.gz
rm 01_fastqc_after/*.zip

# Perform cleanup of intermediate .fq.gz files
cd ..
rm 01_adapters_removed/*fastq.gz 01_adapters_removed/*json

echo "Done."
