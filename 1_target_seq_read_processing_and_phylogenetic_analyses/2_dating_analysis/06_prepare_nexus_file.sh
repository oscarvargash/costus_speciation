pxs2nex -s supermatrix.fasta -o supermatrix.nex

#Also copy the following block modified from supermatrix.model into the nexus file

#begin mrbayes;
#	charset cluster8176 = 1-684;
#	charset cluster12573 = 685-1327;
#	charset cluster4129 = 1328-2222;
#	charset cluster7723 = 2223-2819;
#	charset cluster13987 = 2820-3475;
#	charset cluster12457 = 3476-3826;
#	charset cluster5440 = 3827-4962;
#	charset cluster3407 = 4963-5770;
#	charset cluster2807 = 5771-5891;
#	charset cluster6003 = 5892-6645;
#	charset cluster3811 = 6646-7924;
#	charset cluster5780 = 7925-8604;
#	charset cluster3939 = 8605-9517;
#	charset cluster15387 = 9518-10206;
#	charset cluster6328 = 10207-11474;
#	charset cluster13724 = 11475-11651;
#	charset cluster2784 = 11652-13010;
#	charset cluster2649 = 13011-13895;
#	charset cluster2434 = 13896-15737;
#	charset cluster4146 = 15738-16278;
#	charset cluster7943 = 16279-17368;
#	charset cluster11757 = 17369-17589;
#	charset cluster3680 = 17590-18137;
#	charset cluster6032 = 18138-19324;
#	charset cluster5232 = 19325-20293;
#	charset cluster2555 = 20294-21439;
#	charset cluster4298 = 21440-22237;
#end;


