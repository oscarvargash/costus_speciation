for file in *.fasta; do mafft --thread 7 --localpair --maxiterate 1000 $file > $file.mafft; done

for file in *.mafft; do pxclsq -s $file -p 0.95 -o $file.oc; done

