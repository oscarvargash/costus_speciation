#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import glob
import subprocess

#get a list of prefixes for the sampling
files_for_prefix = glob.glob("*_R1.fastq.gz")
samples = map(lambda each:each.split("_R1")[0], files_for_prefix)

markers_files = glob.glob("*.longest.rn")
markers = map(lambda each:each.split(".")[0], markers_files)

for marker in markers_files:
    for sample in samples:
        pe1 = str(sample) + "_R1.fastq.gz"
        pe2 = str(sample) + "_R2.fastq.gz"
        marker_prefix = marker.split("_")[0]
        out = str(marker_prefix) + "_" + str(sample) + ".sam"
        ref = marker
        null = "null"
        cmd = ["bbwrap.sh"," in1=",pe1,",",null," in2=",pe2,",",null,\
        " outm=",out," append"," ref=",ref," nodisk -Xmx6g "," bamscript=bs.sh;"," sh bs.sh;"\
        " sam2consensus.py -i ",out]
        cmd = "".join(cmd)
        print cmd
        process = subprocess.Popen(cmd.split(),stdout=subprocess.PIPE)
        output, error = process.communicate()
        

print "done (っ▀¯▀)つ"
