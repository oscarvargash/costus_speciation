
import shutil
import pandas as pd
from Bio import AlignIO
import glob
import os

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


print "creating informative dataframe"

outgroups = ["Maranta_leuconeura", "Strelitzia_reginae", "Curcuma_longa", "Zingiber_officinale", "Canna_sp",
            "Heliconia_sp", "Typha_latifolia", "Dichorisandra_thyrsiflora", "Musa_basjoo", "Orchidantha_fimbriata",
            "Canna_indica"]

# create a data frame for clusters
fasta_files = glob.glob("*_zing.fasta.mafft.oc")
clusters = map(lambda each: each.split("_")[0], fasta_files)

columns = ["cluster", "length", "number_of_seq", "contains_Typha_outgroup", "number_of_outgroups"]
stats = pd.DataFrame(columns=columns)
clusters.sort()
stats['cluster'] = clusters
stats = stats.fillna(0)
stats.set_index('cluster', inplace=True)

print "Populating dataframe"

for file in fasta_files:
    print "reading file " + file
    prefix = file.split("_")[0]
    alignment = AlignIO.read(file, format="fasta")
    stats.loc[prefix, "length"] = alignment.get_alignment_length()
    stats.loc[prefix, "number_of_seq"] = len(alignment)
    seq_names = []
    for record in alignment:
        seq_names.append(record.id)
    # Check that desired outgroup is in the alignment
    if "Typha_latifolia" in seq_names:
        stats.loc[prefix, "contains_Typha_outgroup"] = 1
    zing_in_fasta = intersection(outgroups, seq_names)
    stats.loc[prefix, "number_of_outgroups"] = len(zing_in_fasta)

stats.to_csv(path_or_buf="gene_occupancies.csv")

print "subselecting genes that contain all the 11 zingiberales"

columns_w_outgroup0 = stats[stats['number_of_outgroups']==11]
columns_w_outgroup = columns_w_outgroup0[columns_w_outgroup0['length']>100]
columns_w_outgroup.to_csv(path_or_buf="gene_w_outgroup_occupancies.csv")

desired_clusters = columns_w_outgroup.index.tolist()
print 'number of genes with all zingiberales:'
print len(desired_clusters)

os.makedirs("beast")
for prefix in desired_clusters:
    file1 = prefix + "_zing.fasta.mafft.oc"
    shutil.copy(file1, "./beast")

shutil.move("gene_w_outgroup_occupancies.csv","./beast")

print "done"

