#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import glob
import shutil
from Bio import SeqIO

print "relocating unnecessary mapping files *.bam *.bai *.sam "

os.makedirs("disposable_mapping_files")
os.makedirs("references")
os.system("mv *.bam*  disposable_mapping_files")
os.system("mv *.sam*  disposable_mapping_files")
os.system("mv *fasta.longest.rn references")

print "renaming sequences inside fastas"

file_of_mapped_fastas = glob.glob("*__*")

for file in file_of_mapped_fastas:
    base1 = file.split('.')[0]
    species = base1.split('_')[-1]
    genus = base1.split('_')[-2]
    newseqname = genus + "_" + species
    print newseqname
    record = SeqIO.read(file, format="fasta")
    record.id = newseqname
    record.name = ""
    record.description = ""
    outfile = file + ".rn"
    with open(outfile, "w") as output_handle:
        SeqIO.write(record, output_handle, "fasta")
    shutil.move(file, "disposable_mapping_files")

print "concatenating fasta files"

files_for_prefix = glob.glob("*supercontig.fasta.mafft.trim.spur.ts.rd")
clusters = map(lambda each:each.split("_")[0], files_for_prefix)

for cluster in clusters:
    os.system('cat *'+cluster+'* > ' + cluster + "_zing.fasta")

os.system("mv *fasta.rn disposable_mapping_files")
os.system("mv *spur.ts.rd references")

print "done (っ▀¯▀)つ"
