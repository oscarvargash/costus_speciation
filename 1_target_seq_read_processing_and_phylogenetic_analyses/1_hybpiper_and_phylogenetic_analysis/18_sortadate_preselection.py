#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Phyton script to analyze gene aligments and trees
for desired outgroup so they can be used in
a sortadata analysis
"""

import glob
import os
import pandas as pd
from Bio import AlignIO
import shutil


# Get a list of clusters for which trees were calculated
os.chdir("./supercontigs/075_75_no_outliers4/shrinked_trees_fastas_01")
treefiles = glob.glob("*"+".treefile.ts.rd")
clusters = map(lambda each: each.split(".treefile")[0], treefiles)
base_alignments = map(lambda each: each + "_supercontig.fasta.mafft.trim.spur.ts", clusters)
alignments = map(lambda each: each + "_supercontig.fasta.mafft.trim.spur.ts.rd", clusters)


#create a data frame for clusters

columns = ["cluster", "length", "number_of_seq","contains_C_luca_outgroup","number_of_outgroups"]

stats = pd.DataFrame(columns=columns)
base_stats = pd.DataFrame(columns=columns)

clusters.sort()

stats['cluster'] = clusters
base_stats['cluster'] = clusters

stats = stats.fillna(0)
base_stats = stats.fillna(0)

stats.set_index('cluster', inplace=True)
base_stats.set_index('cluster', inplace=True)

outgroups = ["C_dubi98074","C_luca99103","C_afer98068"]

print "Populating dataframe"

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

for file in alignments:
    print "reading file " + file
    prefix = file.split("_")[0]
    alignment = AlignIO.read(file, format="fasta")
    stats.loc[prefix, "length"] = alignment.get_alignment_length()
    stats.loc[prefix, "number_of_seq"] = len(alignment)
    seq_names = []
    for record in alignment:
        seq_names.append(record.id)
    # Check that desired outgroup is in the alignment
    if "C_luca99103" in seq_names:
        stats.loc[prefix, "contains_C_luca_outgroup"] = 1
    outs_in_fasta = intersection(outgroups, seq_names)
    stats.loc[prefix, "number_of_outgroups"] = len(outs_in_fasta)

stats.to_csv(path_or_buf="gene_occupancies_rd.csv")

for file in base_alignments:
    print "reading file " + file
    prefix = file.split("_")[0]
    alignment = AlignIO.read(file, format="fasta")
    base_stats.loc[prefix, "length"] = alignment.get_alignment_length()
    base_stats.loc[prefix, "number_of_seq"] = len(alignment)
    seq_names = []
    for record in alignment:
        seq_names.append(record.id)
    # Check that desired outgroup is in the alignment
    if "C_luca99103" in seq_names:
        base_stats.loc[prefix, "contains_C_luca_outgroup"] = 1
    outs_in_fasta = intersection(outgroups, seq_names)
    base_stats.loc[prefix, "number_of_outgroups"] = len(outs_in_fasta)

stats.to_csv(path_or_buf="gene_occupancies.csv")


#select only the genes with the desired outgroup:
columns_w_outgroup = stats[stats['contains_C_luca_outgroup']==1]
columns_w_outgroup.to_csv(path_or_buf="gene_w_outgroup_occupancies.csv")

desired_clusters = columns_w_outgroup.index.tolist()

os.makedirs("sortadate")
for prefix in desired_clusters:
    file1 = prefix + ".treefile.ts.rd"
    file2 = prefix + "_supercontig.fasta.mafft.trim.spur.ts.rd"
    shutil.copy(file1, "./sortadate")
    shutil.copy(file2, "./sortadate")

shutil.move("gene_w_outgroup_occupancies.csv","./sortadate")

print "done (っ▀¯▀)つ"

#os.chdir("../..")