#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import argparse
from Bio import SeqIO
import glob

####### Arguments and help ###########
parser = argparse.ArgumentParser(description="\
Script to remove the cluster number from fasta files (or any suffix) \
the script will take keep the characters before the separator of your choice\
written by Oscar Vargas oscarmvargas.com\
")
parser.add_argument("-i", "--input", help="ending pattern input files, required", type=str, required = True)
parser.add_argument("-s", "--separator", help="word separator, default = - ", default = "-", type=str)
parser.parse_args()
args = parser.parse_args()

file_suffix = args.input
separator = args.separator
######################################

files = glob.glob("*"+file_suffix)

for file in files:
	print file

	records = list(SeqIO.parse(file,format= "fasta"))
	for record in records:
		seq_name = str(record.name)
		new_seq_name = seq_name.split(separator)[0]
		record.id = new_seq_name
		record.name = ""
		record.description = ""
		with open(file, "w") as output_handle:
			SeqIO.write(records,output_handle,"fasta")

print "fasta names are fixed in their orginal files"