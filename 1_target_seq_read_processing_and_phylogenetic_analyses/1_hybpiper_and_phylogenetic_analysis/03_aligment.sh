# make aligments
cd exons
for file in *.FNA; do mafft --thread 7 --localpair --maxiterate 1000 $file > $file.mafft; done
for file in *mafft; do trimal -in $file -out $file.trim -automated1; done

cd ../introns
for file in *.fasta; do mafft --thread 7 --localpair --maxiterate 1000 $file > $file.mafft; done
for file in *mafft; do trimal -in $file -out $file.trim -automated1; done

cd ../supercontigs
for file in *.fasta; do mafft --thread 14 --localpair --maxiterate 1000 $file > $file.mafft; done
for file in *mafft; do trimal -in $file -out $file.trim -automated1; done
for file in *trim; do trimal -in $file -out $file.spur -resoverlap 0.75 -seqoverlap 75; done


