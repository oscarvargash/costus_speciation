#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import os, sys
import pandas as pd
from Bio import AlignIO
import shutil
import argparse

####### Arguments and help ###########
parser = argparse.ArgumentParser(description="\
this script takes a file in which each line contains a file pattern desired\
then it looks for for file in the folder with that pattern\
and creates a subselction folder with a copy of the files contianing desiring patterns \
written by Oscar Vargas oscarmvargas.com\
")
parser.add_argument("-i", "--input", help="file contianing a list of patterns desired", type=str, required=True)
parser.add_argument("-f", "--folder", help="folder with files for subselection", type=str, default=".")
parser.add_argument("-o", "--output", help="output folder where copied subselected files will be located", type=str, default="subselection")
parser.parse_args()
args = parser.parse_args()

pattern_file = args.input
working_folder = args.folder
output_folder = args.output
######################################

print "reading imput file and creating an internal list of desired patterns"
desired_patterns = []
with open(pattern_file) as infile:
    for line in infile:
        cluster = line.split('\n')[0]
        desired_patterns.append(cluster)

print "list of desired patterns created:"
print desired_patterns

print 'now copying files that include desired pattern into: ' + output_folder
os.makedirs(output_folder)

for pattern in desired_patterns:
    command = 'cp *' + pattern + '* ' + output_folder
    print command
    os.system(command)

print "done (っ▀¯▀)つ"
