# Obtain summary statistics
python ../hybpiper_stats.py test_seq_lengths.txt namelist.txt > test_stats.txt

#Make heatmat
Rscript gene_recovery_heatmap.R

# Retrive sequences
python ../retrieve_sequences.py targets.fasta . dna
mkdir exons
mv *.FNA exons/

python ../retrieve_sequences.py targets.fasta . intron
mkdir introns
mv *_introns.fasta introns/

cd introns
python ../fasta_seq_cluster_name_removal.py -i .fasta -s -

python ../retrieve_sequences.py targets.fasta . supercontig
mkdir supercontigs
mv *_supercontig.fasta supercontigs/

cd supercontigs
python ../fasta_seq_cluster_name_removal.py -i .fasta -s -
