#!/usr/bin/env python
# -*- coding: utf-8 -*-

# code to remove sequences from inside multiple fasta ans tree files

import glob
from Bio import SeqIO


print "leaving one sample per species inside alignments"

file_pattern = "_supercontig.fasta.mafft.trim.spur.ts"
all_files = glob.glob('*'+file_pattern)

seq_to_remove = ["C_dirz19085","C_dubi19268","C_dubi19274","C_AFer19206","C_clav19256","C_glau19166","C_guan18079","C_guan18056","C_guan18002","C_brac18029","C_laev18031","C_laev19191","C_laevRD299","C_ricu19218","C_lasi19117","C_lima18035","C_lima19228","C_long18061","C_malo19216","C_mont18037","C_osae18038","C_pict19284","C_plic19067","C_prod19263","C_prod19281","C_pulv18040","C_pulv18041","C_pulv18081","C_pulv18082","C_pulv18083","C_pulv18084","C_pulv19119","C_puxs19208","C_AFsc18060","C_scab18025","C_scab18045","C_scab18046","C_scab18078","C_scab18086","C_scab18087","C_scab19167","C_spno19122","C_AFwi18020","C_pict19118","C_spic19272","C_spir18088","C_spir19077","C_sten18048","C_varz98076","C_vill19169","C_vill98048","C_vino98171","C_wils18051","C_wood18089","C_wood19232"]

files_with_unwanted_seq = []

for file in all_files:
    print "processing " + file
    outfile = file + ".rd"
    records = list(SeqIO.parse(file,format= "fasta"))
    records_keep =[]
    for record in records:
        if str(record.name) not in seq_to_remove:
            #print str(record.name)
            records_keep.append(record)
        else:
            files_with_unwanted_seq.append(file)
            print str(record.name)
    with open(outfile, "w") as output_handle:
        SeqIO.write(records_keep, output_handle, "fasta")


print str(seq_to_remove) + " sequences were removed from " + str(files_with_unwanted_seq)

