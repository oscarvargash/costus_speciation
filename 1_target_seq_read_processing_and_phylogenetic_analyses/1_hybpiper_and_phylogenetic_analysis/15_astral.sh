# run astral without collapsing unsuported branches
java -jar astral.5.6.3.jar -i alltrees.ts -o astral.tre

# collapse branches with less than 90% bootstrap
nw_ed alltrees.ts 'i & b<=90' o > alltrees_BS90.tre

# run ASTRAL
java -jar astral.5.6.3.jar -i alltrees_BS90.tre -o astral_BS90.tre
