#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python code to allocate outlier alignments and trees in a folder

import glob
import os
import shutil
import subprocess

# create a list of clusters present in the folder

file_pattern = "_supercontig.fasta.mafft.trim.spur.ts"
all_files = glob.glob('*'+file_pattern)
all_prefix = map(lambda each: each.split('_')[0], all_files)

print "reading file with outlier clusters"

with open("quantiles_90_outliers.txt") as infile:
    clusters_to_remove = []
    for line in infile:
        if "cluster" in line:
            cluster_name = line.split(".")[0]
            clusters_to_remove.append(cluster_name)
            print cluster_name


print "moving files to ./outliers_90"

os.makedirs("outliers_90")

for cluster in clusters_to_remove:
    file1 = cluster + ".treefile_0.10.norr"
    file2 = cluster + "_supercontig.fasta.mafft.trim.spur.ts"
    file3 = cluster + ".treefile_RS_0.10.txt"
    shutil.move(file1, "./outliers_90")
    shutil.move(file2, "./outliers_90")
    shutil.move(file3, "./outliers_90")



