#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
this script will select the files associated with the genes
found by sortadate and will create a subselection and an
information table
"""

import glob
import os
import pandas as pd
from Bio import AlignIO
import shutil

## code for develoment only
#os.chdir('./supercontigs/033_75_no_outliers4/shrinked_trees_fastas_01/sortadate')
##

print "Importing file with gene stats"
all_stats = pd.read_csv("gene_w_outgroup_occupancies.csv")
all_stats.set_index('cluster', inplace=True)
all_stats['root_to_tip_var'], all_stats['treelength'], all_stats['bipartition'] = [0,0,0]


print "creating a dataframe with info about selected genes"

sortadate_clusters = []
with open("gg_20") as infile:
    for line in infile:
        if "cluster" in line:
            line = line.split('\n')[0]
            cluster = line.split('.')[0]
            all_stats.loc[cluster, 'root_to_tip_var'] = line.split(' ')[1]
            all_stats.loc[cluster, 'treelength'] = line.split(' ')[2]
            all_stats.loc[cluster,'bipartition'] = line.split(' ')[3]
            sortadate_clusters.append(cluster)
            #print cluster + str(root_to_tip_var) + str(treelength) + str(bipartition)


sortadate_clusters_stats = all_stats[all_stats['treelength']!=0]

print "copying selected fastas and trees to selection folder"

os.makedirs("selection")
for prefix in sortadate_clusters:
    file1 = prefix + ".treefile.ts.rd.rr"
    file2 = prefix + "_supercontig.fasta.mafft.trim.spur.ts.rd"
    shutil.copy(file1, "./selection")
    shutil.copy(file2, "./selection")


sortadate_clusters_stats.to_csv(path_or_buf="sortadate_cluster_stats.csv")
shutil.move("sortadate_cluster_stats.csv","./selection")

print "done (っ▀¯▀)つ"
