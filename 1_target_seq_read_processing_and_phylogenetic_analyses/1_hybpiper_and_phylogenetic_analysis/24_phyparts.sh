java -jar ../target/phyparts-0.0.1-SNAPSHOT-jar-with-dependencies.jar -a 1 -v -d trees -m sm_075_75_010_010.treefile.rr.ns -o out00
#Now Phyparts
xvfb-run python ../phypartspiecharts.py --svg_name pies_00.svg --no_ladderize sm_075_75_010_010.treefile.rr.ns out00 752

java -jar ../target/phyparts-0.0.1-SNAPSHOT-jar-with-dependencies.jar -a 1 -v -s 90 -d trees -m sm_075_75_010_010.treefile.rr.ns -o out90
#Now Phyparts
xvfb-run python ../phypartspiecharts.py --svg_name pies_90.svg --no_ladderize sm_075_75_010_010.treefile.rr.ns out90 752

java -jar ../target/phyparts-0.0.1-SNAPSHOT-jar-with-dependencies.jar -a 1 -v -s 60 -d trees -m sm_075_75_010_010.treefile.rr.ns -o out60
#Now Phyparts
xvfb-run python ../phypartspiecharts.py --svg_name pies_60.svg --no_ladderize sm_075_75_010_010.treefile.rr.ns out60 752