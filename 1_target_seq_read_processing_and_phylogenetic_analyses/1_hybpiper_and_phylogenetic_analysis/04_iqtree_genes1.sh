for prefix in $(ls *1_supercontig.fasta.mafft.trim.spur | sed -e "s/\_.*$//"); do iqtree -nt AUTO -ntmax 6 -m GTR+G -bb 1000 -s $prefix'_supercontig.fasta.mafft.trim.spur' -pre $prefix; done
for prefix in $(ls *2_supercontig.fasta.mafft.trim.spur | sed -e "s/\_.*$//"); do iqtree -nt AUTO -ntmax 6 -m GTR+G -bb 1000 -s $prefix'_supercontig.fasta.mafft.trim.spur' -pre $prefix; done
command; echo "genes 1" | mail -s "Process done" oscarvargash@gmail.com


