#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Python to removes sequences from inside multiple fasta files
# this code replaces original file, back up original files just in case


import glob
from Bio import SeqIO

file_pattern = "_supercontig.fasta.mafft.trim.spur"
all_files = glob.glob('*'+file_pattern)

seq_to_remove = ["C_AFla18054","C_AFsp18047","C_AFwo18055","C_eryt18057","C_laev18034","C_spic19250","C_spir98161","C_varg19019","C_phae19270","C_char19255","C_beck19080","C_cord19084","C_eryt19133","C_geot19088","C_guan19134","C_lasi19141","C_leuc19145","C_pict19138","C_plow19102","C_quas19139","C_curv19278","C_geot19279","C_juru19259","C_juru19280","C_arab18053","C_pict19147"]

files_with_unwanted_seq = []

for file in all_files:
    print "processing " + file
    records = list(SeqIO.parse(file,format= "fasta"))
    records_keep =[]
    for record in records:
        if str(record.name) not in seq_to_remove:
            #print str(record.name)
            records_keep.append(record)
        else:
            files_with_unwanted_seq.append(file)
            print str(record.name)
    with open(file, "w") as output_handle:
        SeqIO.write(records_keep, output_handle, "fasta")


print str(seq_to_remove) + " sequences were removed from " + str(files_with_unwanted_seq)

