The pipeline used for gene selection and bait desing was written by Edgardo Ortiz and can be accessed upon request https://github.com/edgardomortiz

For this Costus study we run the code with all the default parameters with the exception of the last two steps where genes and selected and baits are designed from these:

python baiter_development/d5_select_clusters.py --len 720,2400 --spp 4,6 --foc 1 --gap 0,5 --pid 75,99.6 --psr 0,15

python baiter_development/d6_create_baits.py --manually-selected-folder functional_genes_to_sequence_costus --exclude-reference 00_genomic_data/Costus_pulverulentus_chloroplast.fasta.gz --gc 30,70 --tpo 66.66

