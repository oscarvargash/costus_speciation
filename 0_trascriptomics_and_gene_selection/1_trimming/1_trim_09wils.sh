

seqyclean -qual 20 20 -minlen 25 -polyat -c Illumina_contaminants.fa -1 9wils_S104_L008_R1_001.cor.fq.gz -2 9wils_S104_L008_R2_001.cor.fq.gz -o filtered/09wils

cd ./filtered

filterbyname.sh in=09wils_SE.fastq out=09wils_SE1.fastq substring=t include=t names=1:N:0:
filterbyname.sh in=09wils_SE.fastq out=09wils_SE2.fastq substring=t include=t names=2:N:0:
#cat 09wils_PE1.fastq 09wils_SE1.fastq > 09wils_PE_SE_1.fastq
#cat 09wils_PE2.fastq 09wils_SE2.fastq > 09wils_PE_SE_2.fastq

command; echo "Process 09wils done" | mail -s "Process done" oscarvargash@gmail.com

cd ..


