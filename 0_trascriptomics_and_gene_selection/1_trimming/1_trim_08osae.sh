

seqyclean -qual 20 20 -minlen 25 -polyat -c Illumina_contaminants.fa -1 8osae_S103_L008_R1_001.cor.fq.gz -2 8osae_S103_L008_R2_001.cor.fq.gz -o filtered/08osae

cd ./filtered

filterbyname.sh in=08osae_SE.fastq out=08osae_SE1.fastq substring=t include=t names=1:N:0:
filterbyname.sh in=08osae_SE.fastq out=08osae_SE2.fastq substring=t include=t names=2:N:0:
#cat 08osae_PE1.fastq 08osae_SE1.fastq > 08osae_PE_SE_1.fastq
#cat 08osae_PE2.fastq 08osae_SE2.fastq > 08osae_PE_SE_2.fastq

command; echo "Process 08osae done" | mail -s "Process done" oscarvargash@gmail.com

cd ..


