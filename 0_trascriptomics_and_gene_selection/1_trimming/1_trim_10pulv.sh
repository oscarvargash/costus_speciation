

seqyclean -qual 20 20 -minlen 25 -polyat -c Illumina_contaminants.fa -1 10pulv_S105_L008_R1_001.cor.fq.gz -2 10pulv_S105_L008_R2_001.cor.fq.gz -o filtered/10pulv

cd ./filtered

filterbyname.sh in=10pulv_SE.fastq out=10pulv_SE1.fastq substring=t include=t names=1:N:0:
filterbyname.sh in=10pulv_SE.fastq out=10pulv_SE2.fastq substring=t include=t names=2:N:0:
#cat 10pulv_PE1.fastq 10pulv_SE1.fastq > 10pulv_PE_SE_1.fastq
#cat 10pulv_PE2.fastq 10pulv_SE2.fastq > 10pulv_PE_SE_2.fastq

command; echo "Process 10pulv done" | mail -s "Process done" oscarvargash@gmail.com

cd ..

