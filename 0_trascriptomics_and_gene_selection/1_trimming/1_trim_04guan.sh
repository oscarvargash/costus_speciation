seqyclean -qual 20 20 -minlen 25 -polyat -c Illumina_contaminants.fa -1 4guan_S102_L008_R1_001.cor.fq.gz -2 4guan_S102_L008_R2_001.cor.fq.gz -o filtered/04guan

cd ./filtered

filterbyname.sh in=04guan_SE.fastq out=04guan_SE1.fastq substring=t include=t names=1:N:0:
filterbyname.sh in=04guan_SE.fastq out=04guan_SE2.fastq substring=t include=t names=2:N:0:
#cat 04guan_PE1.fastq 04guan_SE1.fastq > 04guan_PE_SE_1.fastq
#cat 04guan_PE2.fastq 04guan_SE2.fastq > 04guan_PE_SE_2.fastq

command; echo "Process 04guan_PE1 done" | mail -s "Process done" oscarvargash@gmail.com

cd ..


