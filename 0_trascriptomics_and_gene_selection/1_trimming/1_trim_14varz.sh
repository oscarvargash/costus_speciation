

seqyclean -qual 20 20 -minlen 25 -polyat -c Illumina_contaminants.fa -1 14varz_S106_L008_R1_001.cor.fq.gz -2 14varz_S106_L008_R2_001.cor.fq.gz -o filtered/14varz

cd ./filtered

filterbyname.sh in=14varz_SE.fastq out=14varz_SE1.fastq substring=t include=t names=1:N:0:
filterbyname.sh in=14varz_SE.fastq out=14varz_SE2.fastq substring=t include=t names=2:N:0:
#cat 14varz_PE1.fastq 14varz_SE1.fastq > 14varz_PE_SE_1.fastq
#cat 14varz_PE2.fastq 14varz_SE2.fastq > 14varz_PE_SE_2.fastq

command; echo "Process 14varz done" | mail -s "Process done" oscarvargash@gmail.com

cd ..

