#!/usr/bin/python

#program to modify create a .pbs TRINITY assembly file for each of the samples

import pandas as pd
import os


acc = pd.read_csv('accesion_correspondences_v2.csv')
acc.tail

#make duplicate of trinity_82415.pbs for every accesion, we will later modify the accession number

for index, row in acc.iterrows():
	lines = []
	file = "2_trinity_" + row['Taxon_ID'] + '.sh'
	with open('2_trinity_blank.sh') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#Now we need to change the ID inside every file to match their ID in the 
#file name

for index, row in acc.iterrows():
	lines = []
	file = "2_trinity_" + row['Taxon_ID'] + '.sh'
	with open(file) as infile:		
		for line in infile:
			replacements = {'82415': row['Taxon_ID']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#now we are going to create the sample_files necessary for every run
#lets duplicate the files
acc_illu = acc.head(16)
for index, row in acc_illu.iterrows():
	lines = []
	file = "files_" + row['Taxon_ID'] + '.txt'
	with open('files_blank.txt') as infile:		
		for line in infile:
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

#lets change its contents to match each sample

for index, row in acc_illu.iterrows():
	lines = []
	file = "files_" + row['Taxon_ID'] + '.txt'
	with open(file) as infile:		
		for line in infile:
			replacements = {'82415': row['Taxon_ID']}
			for src, target in replacements.iteritems():
				line = line.replace(src, target)
			lines.append(line)
	with open(file, 'w') as outfile:
		for line in lines:
				outfile.write(line)

