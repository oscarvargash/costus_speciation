

# Run Trinity
Trinity --seqType fq --max_memory 96G --CPU 7 --full_cleanup --SS_lib_type RF --output 10pulv.trinity --run_as_paired --samples_file files_10pulv.txt

# Save results
cp *.fasta /home/brlab/omv_data/data/filtered

# send e-mail when finished
command; echo "Process trinity_10pulv done" | mail -s "Process done" oscarvargash@gmail.com


