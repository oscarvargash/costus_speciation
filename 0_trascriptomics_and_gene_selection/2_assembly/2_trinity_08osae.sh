

# Run Trinity
Trinity --seqType fq --max_memory 96G --CPU 7 --full_cleanup --SS_lib_type RF --output 08osae.trinity --run_as_paired --samples_file files_08osae.txt

# Save results
cp *.fasta /home/brlab/omv_data/data/filtered

# send e-mail when finished
command; echo "Process trinity_08osae done" | mail -s "Process done" oscarvargash@gmail.com


